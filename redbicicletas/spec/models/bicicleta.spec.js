var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', () => {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach( (done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('Conexion a la base de datos establecida!');
            done();
        });
    });

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) {console.log(err);}
            mongoose.connection.close(done);
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'rojo', 'urbana', [3.5,-76.5]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(3.5);
            expect(bici.ubicacion[1]).toEqual(-76.5);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agregar una Bicicleta', (done) => {    
            var oBici = new Bicicleta({
                code: 1,
                color: 'rojo',
                modelo: 'urbana'
            });

            Bicicleta.add(oBici, function (err, newBici) {
                if (err) {
                    console.log(err);
                }

                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(oBici.code);

                    done();
                });
            });
        });
    }); 

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var obici1 = new Bicicleta({
                    code: 1,
                    color: 'rojo',
                    modelo: 'urbana'
                });

                Bicicleta.add(obici1, function (err, newBici) {
                    if (err) {console.log(err); }

                    var obici2 = new Bicicleta({
                        code: 2,
                        color: 'verde',
                        modelo: 'montaña'
                    });

                    Bicicleta.add(obici2, function (err, newBici) {
                        if (err) {console.log(err);}
                        
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toBe(obici1.code);
                            expect(targetBici.color).toBe(obici1.color);
                            expect(targetBici.modelo).toBe(obici1.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', ()=> {
        it('Debe borrar la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var obici = new Bicicleta({
                    code: 1,
                    color: 'azul',
                    modelo: 'montaña'
                });

                Bicicleta.add(obici, function (err, newBici) {
                    if (err) {console.log(err);}

                    Bicicleta.removeByCode(1, function (err) {
                        if (err) {console.log(err);}

                        Bicicleta.allBicis(function (err, newBicis) {
                            expect(newBicis.length).toBe(0);

                            done();
                        });
                    });
                });
            });
        });
    });
});